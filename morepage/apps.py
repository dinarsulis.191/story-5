from django.apps import AppConfig


class MorepageConfig(AppConfig):
    name = 'morepage'
