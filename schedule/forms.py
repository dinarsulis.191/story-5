from django import forms
# from .models import Schedule

class ScheduleForm(forms.Form) :
    matkul = forms.CharField(label="Mata Kuliah", max_length=80)
    dosen = forms.CharField(label="Nama Dosen", max_length=40)
    sks = forms.IntegerField(label="Jumlah SKS")
    deskripsi = forms.CharField(label="Deskripsi", max_length=100)
    ruang = forms.CharField(label="Ruang Kelas", max_length=40)

    GANJIL1 = "Ganjil 2019/2020"
    GENAP1 = "Genap 2019/2020"
    GANJIL2 = "Ganjil 2020/2021"
    GENAP2 = "Genap 2020/2021"
    tahunAjaran = [(GENAP1, 'Genap 2019/2020'), (GANJIL1, 'Ganjil 2019/2020'), 
        (GANJIL2, 'Ganjil 2020/2021'), (GENAP2, 'Genap 2020/2021')]
    tahun = forms.ChoiceField(choices=tahunAjaran, label="Semester")