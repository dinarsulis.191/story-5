# Generated by Django 3.1.1 on 2020-10-15 11:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedule', '0008_auto_20201015_1052'),
    ]

    operations = [
        migrations.RenameField(
            model_name='schedule',
            old_name='kelas',
            new_name='matkul',
        ),
        migrations.AddField(
            model_name='schedule',
            name='deskripsi',
            field=models.TextField(default='SOME STRING', max_length=100),
        ),
    ]
