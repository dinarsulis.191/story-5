from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'home.html')

def home(request):
    return render(request, 'home.html')

def about(request):
    return render(request, 'about.html')

def favorite(request):
    return render(request, 'favorite.html')

def experience(request):
    return render(request, 'experience.html')

def contact(request):
    return render(request, 'contact.html')