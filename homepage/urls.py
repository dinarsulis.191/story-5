from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('home/', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('favorite/', views.favorite, name='favorite'),
    path('experience/', views.experience, name='experience'),
    path('contact/', views.contact, name='contact'),
]